name := "SparkSandbox"

version := "0.1"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "1.6.0" % "provided"

libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "1.6.0"

libraryDependencies += "org.apache.spark" % "spark-hive_2.11" % "1.6.0"

libraryDependencies += "org.apache.hadoop" % "hadoop-streaming" % "2.6.0" % "provided"

libraryDependencies += "com.googlecode.json-simple" % "json-simple" % "1.1"

assemblyJarName in assembly := s"${name.value.replace(' ','-')}-${version.value}.jar"

assemblyOption in assembly := (assemblyOption in assembly).
                                    value.copy(includeScala = false)