Spark installation steps:
1. (64 bit) Download and install cygwin, jdk 8, maven 3.3+, sbt 0.13.9, scala 2.11.7, scalaIDE and spark 1.6.0(source code)
2. Set java_home, scala_home, sbt_home, spark_home and add the path of each bin folder to PATH
3. Place winutils.exe into spark/bin and set hadoop_home to the spark directory
4. I have some issues with my %TEMP% folder, so I change it to C:\Users\HenryLie\TEMP and add permission to write to all users
5. Build spark. To produce a Spark package compiled with Scala 2.11, use the -Dscala-2.11 property:
cd /cygdrive/d/PortableSoftware/spark-1.6.0 
./dev/change-scala-version.sh 2.11
mvn -Pyarn -Phadoop-2.6 -Dscala-2.11 -DskipTests clean package
6. Run spark-shell to test installation
spark-shell.cmd

7. If you are behind proxy, set sbt proxy 
http://stackoverflow.com/questions/13803459/how-to-use-sbt-from-behind-proxy

8. For building project in eclipse, remember to modify build.sbt to include spark-core
and then run sbt clean compile eclipse package 

http://spark.apache.org/docs/latest/api/scala/index.html