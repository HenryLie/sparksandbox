package examples

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object NumberSequence {
  def main(args: Array[String]) {
    //create spark config
    val conf = new SparkConf()
      .setAppName("NumberSequence")
      .setMaster("local[2]")  //run with 2 threads
      .set("spark.executor.memory", "1g");
    
    //starting Spark Context
    val sc = new SparkContext(conf)
    
 
     //create RDD containing numbers from 1 to 10
    val numbers = sc.parallelize(1 to 20) 
    println
    println("numbers:")    
    numbers.foreach(println)

    //-------------- TRANSFORMATIONS ------------------
    //filter only even numbers 2,4,6,8,10,...
    val evenNumbers = numbers.filter { num => num%2==0 }
    val evenNumbersGreaterThan5 = evenNumbers.filter { num => num > 10 } 
    println
    println("evenNumbers:")    
    evenNumbers.foreach(println)    
    
//    //run map using lambda
//    val squares = evenNumbers.map(x=> x*x) 
//    println("squaresWithLambda:")
//    squares.foreach(println)
    
    //run map using named function
    def squareFunc(input:Int) : Int = {
      input*input
    }  
    val squares = evenNumbers.map(squareFunc)  // run lambda to calculate square
    println("squaresWithFunc:")    
    squares.foreach(println)
      
    
    //-------------- ACTIONS ------------------
    println
    //collect the data from the workers back to the driver, be careful of out of memory
    val collectedNbr = squares.collect
    println("collectedNumbers:" + collectedNbr.toList.mkString(","))     
    
    //Count
    val countOfItems = squares.count()
    println("Count of items:"+countOfItems)
    
    //Take(n)
    val collectedTwoItems = squares.take(2);
    println("collectedTwoItems:" + collectedTwoItems.toList.mkString(","))
    
    //Reduce using commutative+associative function
    val sumByReduce = squares.reduce((accum,x) => accum+x)
    println("SumByReduce: "+ sumByReduce)
      
  }
}