package examples

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField

object SparkSqlTest {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("SparkSqlTest")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)

    val sqlContext = new SQLContext(sc)

    //Alternatively read data from jdbc
    //    val url =
    //      "jdbc:mysql://yourIP:yourPort/test?user=yourUsername;password=yourPassword"
    //    val df = sqlContext
    //      .read
    //      .format("jdbc")
    //      .option("url", url)
    //      .option("dbtable", "people")
    //      .load()    

    //Read data from json
    val peopleDf = sqlContext.read.json("C:/Users/nbkimuz/workspace/sparksandbox/src/test/resources/people.json")
    println("SCHEMA IS:")
    peopleDf.printSchema()
    peopleDf.foreach(println)

    //example converting to rdd
    val peopleRDD = peopleDf.rdd.map {
      row =>
        val name = row.getAs[String]("name")
    }

    // Count people by age
    val countsByAge = peopleDf.groupBy("age").count()
    countsByAge.show()

    //example registering as table and running with sql
    peopleDf.registerTempTable("people")
    val peopleRecords = sqlContext.sql("SELECT name, address.city, address.state FROM people")
    println("Name, City and State:")
    peopleRecords.collect.foreach(println)

    peopleRecords.write.save("C:/Users/nbkimuz/workspace/sparksandbox/src/test/resources/PeopleOutput")
  }
}
