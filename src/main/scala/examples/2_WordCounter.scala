package examples

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions
import org.apache.hadoop.fs.Path

object WordCounter {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("WordCounter")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)
    
    val inputFilePath = "C:/Users/nbkimuz/workspace/sparksandbox/src/test/resources/starwars_intro.txt"
    //need to delete before running
    val outputFilePath = "C:/Users/nbkimuz/workspace/sparksandbox/src/test/resources/WordCountOutput"

    val textFile = sc.textFile(inputFilePath)    //alternatively if hdfs, hdfs://namenode:9000/path/file

    val wronglyTokenizedFileData = textFile.map(line => line.split(" "))
    //use flatMap instead of Map    
    val tokenizedFileData = textFile.flatMap(line => line.split(" "))

    val TokensCache = tokenizedFileData.cache.setName("Tokens cache")

    //returns a tuple of (word, count)
    val countPrep = TokensCache.map(word => (word, 1))
    println("COUNT PREP:")
    countPrep.foreach(println)

    val wordCountPairRDD = countPrep.reduceByKey((accumValue, newValue) => accumValue + newValue)
    println("WORD COUNT:")
    wordCountPairRDD.foreach(println)
    
    val sortedCounts = wordCountPairRDD.sortBy(kvPair => kvPair._2, false)
    sortedCounts.saveAsTextFile(outputFilePath)
    
    while (true) {
      //show the Spark UI
    }

  }
}