package examples

import org.joda.time.Seconds
import org.json.simple.parser.JSONParser;

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object ClosureMishap {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("SparkSqlTest")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)

    val parser = new JSONParser // <-- INSTANTIATED HERE

    val lines = sc.textFile("C:/Users/nbkimuz/workspace/sparksandbox/src/test/resources/people.json")
//    lines.map(line => parser.parse(line)) // <-- IN THE CLOSURE
    
//    A good fix    
//    def jsonParse(line:String) = {
//       val parser = new JSONParser
//       parser.parse(line)
//    }
//    lines.map(jsonParse)
    lines.foreach(println)
    
  }
}