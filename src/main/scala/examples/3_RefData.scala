package examples

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions

object RefDataIsAwesome {
  def main(args: Array[String]) {

    type name = String
    type team = String
    type character = String
    val conf = new SparkConf()
      .setAppName("WordCounter")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)

    //create cob team
    val cobTeam = sc.parallelize[(String, String)](Seq("Henry" -> "COB", 
        "Tomy" -> "COB", "Mohan" -> "COB", "Aish" -> "COB"))

    //create pme team with reversed key
    val pmeTeam = sc.parallelize[(String, String)](Seq("PME" -> "Wailun", 
        "PME" -> "Sky", "PME" -> "Karthik", "PME" -> "Pallani"))

    //correct the key of pmeTeam, and then union with cobTeam
    val refDataTeam = cobTeam.union(pmeTeam.map(person => (person._2, person._1)))

    val awesomePeople = sc.parallelize[(String, String)](Seq("Tomy" -> "Jedi", 
        "Henry" -> "Someone", "Mohan" -> "The God", "Aish" -> "The Intern"))

    //because it literally is
    val only = awesomePeople.join(refDataTeam)
    println("Count of awesome people:" + only.count)

    val hereIsEveryoneAgain = refDataTeam.leftOuterJoin(awesomePeople).map { person =>
      val (name,(team,characterOption)) = person
      val character = characterOption match {
        case Some(ch) => ch + " in " + team + " team"
        case _        => "Another awesome member" + " in " + team + " team"
      }
      (name, character)
    }

    hereIsEveryoneAgain.foreach(println)

  }
}