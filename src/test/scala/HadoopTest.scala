

import org.apache.hadoop.fs.Path

object HadoopTest {
  def main(args: String) {
    val hadoopConf = new org.apache.hadoop.conf.Configuration()
    val hdfs = org.apache.hadoop.fs.FileSystem.get(new java.net.URI("hdfs://localhost:9000"), hadoopConf)
    //    hdfs.delete(new org.apache.hadoop.fs.Path(outputFilePath), true)  //true for recursive

    println("HDFS file before:")
    hdfs.listFiles(new Path("hdfs://localhost:9000/tmp/"), true)
    hdfs.createNewFile(new Path("hdfs://localhost:9000/tmp/sample.txt"))
    println("HDFS files after:")
    hdfs.listFiles(new Path("hdfs://localhost:9000/tmp/"), true)
  }
}